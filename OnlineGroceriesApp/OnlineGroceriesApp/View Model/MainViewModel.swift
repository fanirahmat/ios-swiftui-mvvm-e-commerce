//
//  MainViewModel.swift
//  OnlineGroceriesApp
//
//  Created by Triandy Gunawan Teng on 20/05/24.
//

import SwiftUI

class MainViewModel: ObservableObject {
    static var shared: MainViewModel = MainViewModel()
    
    @Published var txtEmail: String = ""
    @Published var txtPassword: String = ""
    @Published var isShowPassword: Bool = false
}


