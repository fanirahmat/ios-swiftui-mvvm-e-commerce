//
//  RoundedButton.swift
//  OnlineGroceriesApp
//
//  Created by Mohammad Fani Rahmatulloh on 16/05/24.
//

import SwiftUI

struct RoundedButton: View {
    @State var title: String = "Title"
   
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 20)
                .frame(minWidth: 0, maxWidth: .screenWidth, minHeight: 60, maxHeight: 60)
                .foregroundColor(Color.primaryColor)
            Text(title)
                .font(.customfont(.semibold, fontSize: 18))
                .foregroundColor(.white)
                .multilineTextAlignment(.center)
        }
        
    }
}

#Preview {
    RoundedButton()
}
