//
//  LineTextField.swift
//  OnlineGroceriesApp
//
//  Created by Triandy Gunawan Teng on 20/05/24.
//

import SwiftUI

struct LineTextField: View {
    @Binding var txt: String
    
    @State var title: String = "Title"
    @State var placeholder: String = "placeholder"
    @State var keyboardType: UIKeyboardType = .default
   
    var body: some View {
        VStack {
            Text(title)
                .font(.customfont(.semibold, fontSize: 16))
                .foregroundColor(.textTitleColor)
                .frame(minWidth: 0, maxWidth: .screenWidth, alignment: .leading)
            
            TextField(placeholder, text: $txt)
                .font(.customfont(.medium, fontSize: 18))
                .keyboardType(keyboardType)
                .frame(height: 40)
                .autocorrectionDisabled(true)
           
            Divider()
        }
    }
}

struct LineSecureTextField: View {
    @Binding var txt: String
    @Binding var isShowPassword: Bool
    
    @State var title: String = "Title"
    @State var placeholder: String = "placeholder"
    @State var keyboardType: UIKeyboardType = .default
   
    var body: some View {
        VStack {
            Text(title)
                .font(.customfont(.semibold, fontSize: 16))
                .foregroundColor(.textTitleColor)
                .frame(minWidth: 0, maxWidth: .screenWidth, alignment: .leading)
    
            if !isShowPassword {
                SecureField(placeholder, text: $txt)
                    .font(.customfont(.medium, fontSize: 18))
                    .keyboardType(keyboardType)
                    .frame(height: 40)
                    .autocorrectionDisabled(true)
                    .modifier(ShowButton(isShow: $isShowPassword))
            } else {
                TextField(placeholder, text: $txt)
                    .font(.customfont(.medium, fontSize: 18))
                    .keyboardType(keyboardType)
                    .frame(height: 40)
                    .autocorrectionDisabled(true)
                    .modifier(ShowButton(isShow: $isShowPassword))
            }
           
            Divider()
        }
    }
}


