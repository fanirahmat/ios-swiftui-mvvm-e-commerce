//
//  LoginView.swift
//  OnlineGroceriesApp
//
//  Created by Triandy Gunawan Teng on 20/05/24.
//

import SwiftUI

struct LoginView: View {
    
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    //@State var txtEmail: String = ""
    @StateObject var loginVM = MainViewModel.shared
    //@ObservedObject var loginVM = MainViewModel.shared
    
    var body: some View {
        ZStack {
//            Image("bottom_up")
//                .resizable()
//                .scaledToFill()
//                .frame(width: .screenWidth, height: .screenHeight)
            
            VStack {
              
                Image("color_logo")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 40)
                    .padding(.bottom, .screenWidth * 0.1)
                Text("Logging")
                    .font(.customfont(.semibold, fontSize: 26))
                    .foregroundColor(.primaryTextColor)
                    .frame(minWidth: 0, maxWidth: .screenWidth, alignment: .leading)
                    .padding(.bottom, 4)
                Text("Enter your emails and password")
                    .font(.customfont(.semibold, fontSize: 16))
                    .foregroundColor(.primaryTextColor)
                    .frame(minWidth: 0, maxWidth: .screenWidth, alignment: .leading)
                    .padding(.bottom, .screenWidth * 0.1)
                
                LineTextField(
                    txt: $loginVM.txtEmail,
                    title: "Email",
                    placeholder: "Enter your email address",
                    keyboardType: .emailAddress)
                    .padding(.bottom, .screenWidth * 0.07)
                
                
                LineSecureTextField(
                    txt: $loginVM.txtPassword,
                    isShowPassword: $loginVM.isShowPassword,
                    title: "Password", 
                    placeholder: "Enter your password")
                    .padding(.bottom, .screenWidth * 0.02)
                
                Button {
                    
                } label: {
                    Text("Forgot Password")
                        .font(.customfont(.medium, fontSize: 14))
                        .foregroundColor(.primaryTextColor)
                }
                .frame(minWidth: 0, maxWidth: .screenWidth, alignment: .trailing)
                .padding(.bottom, .screenWidth * 0.05)
                
                RoundedButton(title: "Login")
                    .padding(.bottom, .screenWidth * 0.05)
                
                HStack {
                    Text("Don't have an account?")
                        .font(.customfont(.semibold, fontSize: 14))
                        .foregroundColor(.primaryTextColor)
                    Text("Sign up")
                        .font(.customfont(.semibold, fontSize: 14))
                        .foregroundColor(.primaryColor)
                }

                
                Spacer()
            }
            .padding(.top, .topInsets + 64)
            .padding(.horizontal, 20)
            .padding(.bottom, .bottomInsets)
            
            
            VStack {
                HStack {
                    Button(action: {
                        mode.wrappedValue.dismiss()
                    }, label: {
                        Image("back")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 20, height: 20)
                    })
                    Spacer()
                }
                Spacer()
            }
            .padding(.top, .topInsets)
            .padding(.horizontal, 20)
        }
        .background(Color.white)
        .ignoresSafeArea()
        .navigationTitle("")
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
    }
}

#Preview {
    LoginView()
}
