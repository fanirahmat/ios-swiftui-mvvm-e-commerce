//
//  SignInView.swift
//  OnlineGroceriesApp
//
//  Created by Mohammad Fani Rahmatulloh on 16/05/24.
//

import SwiftUI
import CountryPicker

struct SignInView: View {
    
    @State var txtMobile: String = ""
    @State var isShowPicker: Bool = false
    @State var countryObj: Country?
    
    var body: some View {
        ZStack() {
            
            Image("bottom_up")
                .resizable()
                .scaledToFill()
                .frame(width: .screenWidth, height: .screenHeight)
            
            VStack {
                Image("sign_in_top")
                    .resizable()
                    .scaledToFill()
                    .frame(width: .screenWidth, height: .screenWidth)
                Spacer()
            }
            
            ScrollView{
                VStack(alignment: .leading) {
                    Text("Get your groceries with nectar")
                        .font(.customfont(.semibold, fontSize: 26))
                        .foregroundColor(.primaryTextColor)
                        .multilineTextAlignment(.leading)
                        .padding(.bottom, 25)
                    
                    HStack {
                        Button {
                            isShowPicker = true
                        } label: {
                            if let countryObj = countryObj {
                                Text("\(countryObj.isoCode.getFlag())")
                                    .font(.customfont(.medium, fontSize: 35))
                                    
                                Text("+\(countryObj.phoneCode)")
                                    .font(.customfont(.medium, fontSize: 18))
                                    .foregroundColor(.primaryTextColor)
                            }
                            //Image("")
                            
                                
                        }
                        
                        TextField("Enter Mobile", text: $txtMobile)
                            .frame(minWidth: 0, maxWidth: .screenWidth)
                            .font(.customfont(.medium, fontSize: 18))

                    }
                    
                    Divider()
                        .padding(.bottom, 25)
                    
                    Text("Or connect with social media")
                        .font(.customfont(.semibold, fontSize: 14))
                        .foregroundColor(.textTitleColor)
                        .multilineTextAlignment(.center)
                        .frame(minWidth: 0, maxWidth: .screenWidth, alignment: .center)
                        .padding(.bottom, 25)
                    
                    ZStack {
                        RoundedRectangle(cornerRadius: 20)
                            .frame(minWidth: 0, maxWidth: .screenWidth, minHeight: 60, maxHeight: 60)
                            .foregroundColor(Color(hex: "5383EC"))
                        HStack {
                            Image("google_logo")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 20, height: 20)
                            Text("Continue with Google")
                                .font(.customfont(.semibold, fontSize: 18))
                                .foregroundColor(.white)
                                .multilineTextAlignment(.center)
                        }
                    }
                    .padding(.bottom, 8)
                    
                    ZStack {
                        RoundedRectangle(cornerRadius: 20)
                            .frame(minWidth: 0, maxWidth: .screenWidth, minHeight: 60, maxHeight: 60)
                            .foregroundColor(Color(hex: "4A66AC"))
                        HStack {
                            Image("fb_logo")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 20, height: 20)
                            Text("Continue with Facebook")
                                .font(.customfont(.semibold, fontSize: 18))
                                .foregroundColor(.white)
                                .multilineTextAlignment(.center)
                        }
                        
                    }
                }
                .padding(.horizontal, 20)
                .frame(width: .screenWidth, alignment: .leading)
                .padding(.top, .topInsets + .screenWidth * 0.7)
            }
        }
        .ignoresSafeArea()
        .navigationTitle("")
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
        .onAppear {
            self.countryObj = Country(phoneCode: "62", isoCode: "ID")
        }
        .sheet(isPresented: $isShowPicker, content: {
            CountryPickerUI(country: $countryObj)
        })
    }
}

#Preview {
    SignInView()
}
