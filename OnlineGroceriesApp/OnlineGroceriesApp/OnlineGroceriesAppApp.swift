//
//  OnlineGroceriesAppApp.swift
//  OnlineGroceriesApp
//
//  Created by Triandy Gunawan Teng on 16/05/24.
//

import SwiftUI

@main
struct OnlineGroceriesAppApp: App {
    var body: some Scene {
        WindowGroup {
            NavigationView {
                WelcomeView()
            }
        }
    }
}
